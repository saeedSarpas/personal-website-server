var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');

var app = express();

// Express
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// Passport
app.use(session({
  secret: 's3cr3t',
  resave: true,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());


// Routes
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);


// Mongoose
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/mean-social', { useMongoClient: true })
  .then(() =>  console.log('connection successful'))
  .catch((err) => console.error(err));

module.exports = app;
