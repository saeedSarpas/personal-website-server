var express = require('express');
var router = express.Router();

// get user listing as json object
router.get('/', ensureAuthenticated, function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ user: req.user }));
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/auth/login')
}

module.exports = router;
